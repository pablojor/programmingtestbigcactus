﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : Ball
{
    [Tooltip("Collider of the power Up that will trigger other balls")]
    public Collider2D _colliderPU;

    protected override void PlaceOnPosition(Vector2 whereToMove, Collider2D initCollider = null)
    {
        this.transform.position = new Vector3(whereToMove.x, whereToMove.y, this.transform.position.z);
        _collided = true;
        _headCollider.enabled = false;//Moving collider no longer needed
        this.gameObject.transform.rotation = new Quaternion(0, 0, 0, 0);//Reset rotation
        rb.bodyType = RigidbodyType2D.Kinematic;//Set Rigidbody as kinematic to be able of activate triggers
        rb.constraints = RigidbodyConstraints2D.FreezePosition | RigidbodyConstraints2D.FreezeRotation;//Freeze both position and rotation
    }

    /// <summary>
    /// When the powerUp has ended tell the ballContainer to check if it needs to move down
    /// </summary>
    private void OnDestroy()
    {
        _ballContainer.MoveDown();
    }
}
