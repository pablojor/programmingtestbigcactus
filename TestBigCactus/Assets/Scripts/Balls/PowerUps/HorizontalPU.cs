﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorizontalPU : PowerUp
{
    [Tooltip("Second collider that will be moving oposite to the other one")]
    public GameObject son;
    [Tooltip("Speed at which the power up will move")]
    public float speed = 10f;

    //If the powerUp needs move
    private bool _moving = false;
    //If the powerUp has reached the rightLimit
    private bool _rightDone = false;
    //If the powerUp has reached the leftLimit
    private bool _leftDone = false;

    protected override void Update()
    {
        base.Update();
        if (_moving)
        {
            if (this.transform.position.x > -_widthLimit)//If not reached the left limit of the screen move this object to the left
                this.transform.position = new Vector3(this.transform.position.x - speed * Time.deltaTime, this.transform.position.y, this.transform.position.z);
            else
                _leftDone = true;

            if (son.transform.position.x < _widthLimit)//If not reached the right limit of the screen move the second object
                son.transform.position = new Vector3(son.transform.position.x + speed * Time.deltaTime, son.transform.position.y, son.transform.position.z);
            else
                _rightDone = true;

            if (_rightDone && _leftDone)//When both reach their limits, destroy them
            {
                Destroy(this.gameObject);
                Destroy(son);
            }
        }
        else if (_collided)
        {
            _colliderPU.enabled = true;//Enable powerUp collider
            _moving = true;
            son.SetActive(true);//Activate second collider
            son.transform.parent = null;//Detach, so it can freely move
        }
    }
}
