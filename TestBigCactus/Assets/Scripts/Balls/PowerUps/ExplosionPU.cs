﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionPU : PowerUp
{
    [Tooltip("Size that the power will reach before ends")]
    public float size = 10f;
    [Tooltip("Speed at which the power up will expand")]
    public float speed = 5f;

    //If the is growing
    private bool _growing = false;
    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
        if (_growing)
        {
            if (this.gameObject.transform.localScale.x < size)//if size limit not reached make the explosion bigger
                this.gameObject.transform.localScale =new Vector2(this.gameObject.transform.localScale.x + speed * Time.deltaTime, this.gameObject.transform.localScale.y + speed * Time.deltaTime);
            else
                Destroy(this.gameObject);

        }
        else if (_collided)
        {
            _colliderPU.enabled = true;
            _growing = true;
        }
    }
}
