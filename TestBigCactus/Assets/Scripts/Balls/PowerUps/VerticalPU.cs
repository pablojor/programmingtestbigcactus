﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerticalPU : PowerUp
{
    [Tooltip("Speed at which the power up will move")]
    public float speed = 10f;

    //If the powerUp needs move
    private bool _moving = false;
    protected override void Update()
    {
        base.Update();//Call update of base class
        if(_moving)
        {
            if (this.transform.position.y < _topLimit)//Move while the top of the screen is not reached
                this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + speed * Time.deltaTime, this.transform.position.z);
            else
                Destroy(this.gameObject);//otherwise destroy this powerUp

        }
        else if(_collided)
        {
            _colliderPU.enabled = true;//Activate PowerUp collider
            _moving = true;//Start moving
        }
    }
}
