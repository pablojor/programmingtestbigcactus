﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Enum to represent all colors
public enum Colors { Red, Blue, Green };
//Enum to represent all possible adyacent balls
public enum Dirs {TR,TL,R,L,BR,BL};
public class NormalBall : Ball
{
    //Colliders to detect adyacents
    [Tooltip("Collider, in the Top Right of the ball")]
    public Collider2D TopRightCollider;
    [Tooltip("Collider, in the Top Left of the ball")]
    public Collider2D TopLeftCollider;
    [Tooltip("Collider, in the Right of the ball")]
    public Collider2D RightCollider;
    [Tooltip("Collider, in the Left of the ball")]
    public Collider2D LeftCollider;
    [Tooltip("Collider, in the Botton Right of the ball")]
    public Collider2D BottonRightCollider;
    [Tooltip("Collider, in the Botton Left of the ball")]
    public Collider2D BottonLeftCollider;
    [Tooltip("Trigger to detect power Ups collisions")]
    public Collider2D SphereTrigger;

    //If the ball is falling
    private bool _falling = false;
    //If the ball is going to be destroyed
    private bool _destroyed = false;

    //Color associated to the ball
    private Colors _color;
    //Adyacent balls
    private List<NormalBall> _adyacent;
    //The positions of the adyacent balls
    private List<Dirs> _adyacentDirs;
    //If this is and initial ball
    private bool _initialBall = false;
    //Collider of initial Position(So no other ball can get in the same iniPos)
    private Collider2D _initPosCollider;
    //The Graph where this ball is
    private int _graphAssociated = -1;

    //If this ball is marked while doing a DFS
    private bool _markedForDFS = false;

    //Initialize adyacent list
    private void Awake()
    {
        _adyacent = new List<NormalBall>();
        _adyacentDirs = new List<Dirs>();
    }
    /// <summary>
    /// Adds an adyacent ball
    /// </summary>
    /// <param name="ball">The ball to be added</param>
    /// <param name="dir">direction where the adyacent is</param>
    public void AddAdyacent(NormalBall ball, Dirs dir)
    {
        _adyacent.Add(ball);
        _adyacentDirs.Add(dir);
    }
    /// <summary>
    /// Substract an adyacent
    /// </summary>
    /// <param name="index"> the index of the adyacent to be removed</param>
    public void SubstractAdyacent(int index)
    {
        _adyacent.RemoveAt(index);
        _adyacentDirs.RemoveAt(index);
    }

    /// <summary>
    /// If power Up triggers
    /// </summary>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(!_falling && !_destroyed && collision.gameObject.layer == LayerMask.NameToLayer("PowerUp"))//If this ball is not falling, being destroyed and triggered a power up
        {
            _destroyed = true;
            _ballContainer.DestroyBallForPowerUp(this);//Tell the ball container that this ball needs to be destroyed
        }
    }


    /// <summary>
    /// Places a ball on position and activates/deactivates the necesary components
    /// </summary>
    /// <param name="whereToMove"></param>
    protected override void PlaceOnPosition(Vector2 whereToMove, Collider2D initCollider = null)
    {
        if (initCollider != null)//If collided with an init position
        {
            _initPosCollider = initCollider;//Save collider to deactivate it later
            SetInitialBall(true);//Set this ball as an initial
        }

        _collided = true;//This ball has joined the other
        //Activate colliders for possible adyacent balls
        this.SetEnableColliderTR(true);
        this.SetEnableColliderTL(true);
        this.SetEnableColliderR(true);
        this.SetEnableColliderL(true);
        this.SetEnableColliderBR(true);
        this.SetEnableColliderBL(true);
        this.SetSphereTrigger(true);
        //Deactivate the top collider of the box
        this.SetHeadCollider(false);

        this.rb.bodyType = RigidbodyType2D.Static;//This rigidbody will be static now
        transform.rotation = new Quaternion(0, 0, 0, 0);//Reset rotation
        this.transform.position = new Vector3(whereToMove.x, whereToMove.y, this.transform.position.z);//Place the ball in his position

        _ballContainer.AddBallToGraph(this);//Tell the ball container to add this ball to his corresponding graph
    }

    public void CheckCollisionWithInitPos()//Check with a raycast to which initial position this ball is adyacent
    {
        int layerMask = 1 << LayerMask.NameToLayer("InitPos");
        Vector2 dir = new Vector2(0, 1);
        RaycastHit2D hit = Physics2D.Raycast(this.transform.position, dir.normalized, dir.magnitude, layerMask);//Raycast from the new ball to the position
        if (hit.collider != null)//If a initPosition was hit
        {
            _initPosCollider = hit.collider;//Save the collider
        }
    }

    #region CheckersColliderRegion 
    //Methods to check which adyacent collider a ball has hit
    public bool CheckTRCollider(Collider2D collider)
    {
        return collider == TopRightCollider;
    }
    public bool CheckTLCollider(Collider2D collider)
    {
        return collider == TopLeftCollider;
    }
    public bool CheckRCollider(Collider2D collider)
    {
        return collider == RightCollider;
    }
    public bool CheckLCollider(Collider2D collider)
    {
        return collider == LeftCollider;
    }
    public bool CheckBRCollider(Collider2D collider)
    {
        return collider == BottonRightCollider;
    }
    public bool CheckBLCollider(Collider2D collider)
    {
        return collider == BottonLeftCollider;
    }
    #endregion

    #region Getters
    public bool GetInitialBall()
    {
        return _initialBall;
    }
    public ref List<NormalBall> GetAdyacent()
    {
        return ref _adyacent;
    }
    public ref List<Dirs> GetAdyacentDirs()
    {
        return ref _adyacentDirs;
    }
    public Colors GetColor()
    {
        return _color;
    }
    public int GetGraph()
    {
        return _graphAssociated;
    }

    public bool GetCollided()
    {
        return _collided;
    }

    public bool GetMarkedForDFS()
    {
        return _markedForDFS;
    }
    public bool GetFalling()
    {
        return _falling;
    }
    #endregion

    #region Setters


    public void SetInitialBall(bool initialBall)
    {
        _initialBall = true;
    }
    public void SetCollided(bool collided)
    {
        _collided = collided;
    }
    public void SetGraph(int graph)
    {
        _graphAssociated = graph;
    }
    public void SetColor(Colors newColor)
    {
        _color = newColor;

        if (newColor == Colors.Red)
            _renderer.color = Color.red;
        else if(newColor == Colors.Blue)
            _renderer.color = Color.blue;
        else if(newColor == Colors.Green)
            _renderer.color = Color.green;
    }
    public void SetAllColliders(bool enable)
    {
        this.SetEnableColliderTR(enable);
        this.SetEnableColliderTL(enable);
        this.SetEnableColliderR(enable);
        this.SetEnableColliderL(enable);
        this.SetEnableColliderBR(enable);
        this.SetEnableColliderBL(enable);
        this.SetSphereTrigger(enable);
        this.SetHeadCollider(enable);
    }
    public void SetEnableColliderTR(bool enable)
    {
        TopRightCollider.enabled = enable;
    }
    public void SetEnableColliderTL(bool enable)
    {
        TopLeftCollider.enabled = enable;
    }
    public void SetEnableColliderR(bool enable)
    {
        RightCollider.enabled = enable;
    }
    public void SetEnableColliderL(bool enable)
    {
        LeftCollider.enabled = enable;
    }
    public void SetEnableColliderBR(bool enable)
    {
        BottonRightCollider.enabled = enable;
    }
    public void SetEnableColliderBL(bool enable)
    {
        BottonLeftCollider.enabled = enable;
    }
    public void SetSphereTrigger(bool enable)
    {
        SphereTrigger.enabled = enable;
    }
    public void SetHeadCollider(bool enable)
    {
        _headCollider.enabled = enable;
    }
    public void SetMarkedForDFS(bool marked)
    {
        _markedForDFS = marked;
    }
    public void SetInitPosCollider(Collider2D collider)
    {
        _initPosCollider = collider;
    }
    public void SetInitPosColliderEnabled(bool enabled)
    {
        _initPosCollider.enabled = enabled;
    }
    public void SetFalling(bool enabled)
    {
        _falling = enabled;
    }
    #endregion
}
