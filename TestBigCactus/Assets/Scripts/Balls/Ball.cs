﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base class for every ball
/// Both normal game balls and power Ups will inherit from this class
/// </summary>
public class Ball : MonoBehaviour
{
    [Tooltip("Sprite Render of the ball")]
    public SpriteRenderer _renderer;
    [Tooltip("Rigidbody of the ball")]
    public Rigidbody2D rb;
    [Tooltip("Collider to detect with which ball have you collided")]
    public Collider2D _headCollider;

    //If this is ball has joined the others
    protected bool _collided = false;
    //Reference to ball container
    protected BallContainerController _ballContainer;

    //Top limit of the screen
    protected float _topLimit = 10f;
    //width limit of the screen
    protected float _widthLimit = 5f;

    private float _horizontalSeparation = 0.5f;
    private float _verticalSeparation = 0.85f;

    /// <summary>
    /// Every ball that leaves the limits of the map will be destroyed
    /// </summary>
    protected virtual void Update()
    {
        if (!_collided && this.transform.position.y > _topLimit)
            Destroy(this.gameObject);
    }

    /// <summary>
    /// Here if a balls collides with other ball or and initial position, it will be detected
    /// </summary>
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!_collided && collision.otherCollider == _headCollider && collision.collider.gameObject.layer == LayerMask.NameToLayer("StaticBall"))//If the ball is moving and collides with other ball
        {
            NormalBall otherBall = collision.collider.gameObject.GetComponent<NormalBall>();//Get the other ball

            Vector3 otherBallPosition = collision.collider.gameObject.transform.position;
            Vector2 whereToMove = new Vector2(0, 0);

            //Check with which collider of the other ball did we hit
            //Depending on the collider we will be placed in a different place
            if (otherBall.CheckTRCollider(collision.collider))//Top Right collider
            {
                whereToMove.x = _horizontalSeparation;
                whereToMove.y = _verticalSeparation;
            }
            else if (otherBall.CheckTLCollider(collision.collider))//Top Left collider
            {
                whereToMove.x = -_horizontalSeparation;
                whereToMove.y = _verticalSeparation;
            }
            else if (otherBall.CheckRCollider(collision.collider))//Right collider
            {
                whereToMove.x = 1f;
            }
            else if (otherBall.CheckLCollider(collision.collider))//Left collider
            {
                whereToMove.x = -1f;
            }
            else if (otherBall.CheckBRCollider(collision.collider))//Botton Right collider
            {
                whereToMove.x = _horizontalSeparation;
                whereToMove.y = -_verticalSeparation;
            }
            else if (otherBall.CheckBLCollider(collision.collider))//Botton Left collider
            {
                whereToMove.x = -_horizontalSeparation;
                whereToMove.y = -_verticalSeparation;
            }
            else
                return;
            whereToMove.x += otherBallPosition.x;
            whereToMove.y += otherBallPosition.y;

            PlaceOnPosition(whereToMove);
        }
        else if (!_collided && collision.collider.gameObject.layer == LayerMask.NameToLayer("InitPos"))//If the ball is moving and collides with an initial position
        {
            Vector2 whereToMove = new Vector2(collision.collider.gameObject.transform.position.x, collision.collider.gameObject.transform.position.y - 1);
            PlaceOnPosition(whereToMove, collision.collider);
        }
        else if (!_collided)//If the ball is moving and collides with a wall 
        {
            //Rotate the gameobject
            Vector3 moveDirection = rb.velocity.normalized;
            if (moveDirection != Vector3.zero)
            {
                float angle = Mathf.Atan2(moveDirection.y, moveDirection.x) * Mathf.Rad2Deg;
                angle -= 90;
                transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            }
        }
    }
    //Method to be overrided by subclases
    protected virtual void PlaceOnPosition(Vector2 whereToMove, Collider2D initCollider = null)
    {

    }
    //Getter
    public Rigidbody2D GetRigidbody2D()
    {
        return rb;
    }
    //Setter
    public void SetBallContainer(BallContainerController ballContainer)
    {
        _ballContainer = ballContainer;
    }
}
