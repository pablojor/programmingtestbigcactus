﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [Tooltip("Level Manager Script")]
    public LevelManager levelManager;

    private int _lastLevel;
    private bool _playLastLevel = false;
    private bool _lastGameWin = false;

    /// <summary>
    /// Game Manager instance
    /// </summary>
    private static GameManager _instance;

    public static GameManager GetInstance()
    {
        return _instance;
    }
    /// <summary>
    /// Check if exists. if yes, it destroys itself
    /// If there is a LevelManager save it and startGameScene
    /// </summary>
    private void Awake()
    {
        if (_instance != null)
        {
            _instance.levelManager = levelManager;
            if (_instance.levelManager != null)
            {
                _instance.StartGameScene();
            }

            DestroyImmediate(gameObject);
            return;
        }
        _instance = this;
        DontDestroyOnLoad(gameObject);
    }
    /// <summary>
    /// Initialize LevelManager and load map
    /// </summary>
    private void StartGameScene()
    {
        if (levelManager != null)
        {
            levelManager.Init();
            if(_playLastLevel)
                levelManager.LoadMap(_lastLevel);
            else
                levelManager.LoadMap(0);
            _playLastLevel = false;
            _lastLevel = 0;
        }
    }
    /// <summary>
    /// Changes the scene to between levels
    /// </summary>
    /// <param name="lastLevel"> last level played</param>
    /// <param name="win"> if it was a win</param>
    public void EndGame(int lastLevel, bool win)
    {
        _lastGameWin = win;
        _lastLevel = lastLevel;
        SceneManager.LoadScene("BetweenLevels");
    }
    /// <summary>
    /// If the level should be the same
    /// </summary>
    public void PlayLastGame()
    {
        _playLastLevel = true;
    }
    //Getter
    public bool GetWinLastGame()
    {
        return _lastGameWin;
    }
}
