﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    [Tooltip("Min number of rows generating the map")] 
    [Range(1,250)]
    public int minRows;

    [Tooltip("Max number of rows generating the map, Needs to be higher than Min")]
    [Range(1, 250)]
    public int maxRows;

    [Tooltip("Prefab of the ball to create")]
    public NormalBall ballPrefab;

    //Float representing the separation betweem balls in X-axis
    private float _xSeparation = 1f;
    //Float representing the separation betweem balls in y-axis
    private float _ySeparation = 0.85f;

    //Float representing inital position of the balls to create
    private float _initialXPos11 = -5f, _initialXPos10 = -4.5f;
    private float _initialYPos = 9f;

    private BallContainerController _ballContainer;

    /// <summary>
    /// 
    /// The idea is to generate a random number of 11/10 bits.
    /// Those bits represent if there is a ball on each space.
    /// Then we have to create a list representing the row, check if it can be placed under the last row and placed it or generate another one
    /// Repeat for every row in the map
    /// 
    /// </summary>
    /// <param name="newGraphs">List with all the new Graphs that are created after the DFS</param>
    /// <param name="groupNeedsToFall">Bool list asociated to every new Graph, that represents if there is an initial Ball in the group(first row of balls)</param>
    /// <param name="ballConatiner">GameObject where the balls created will be placed</param>
    /// <returns> map number </returns>
    public int ProceduralGeneratedMap(ref List<Graph> newGraphs, ref List<bool> groupNeedsToFall, BallContainerController ballConatiner, int map =0)
    {
        _ballContainer = ballConatiner;
        if (map == 0)
            map = (int)System.DateTime.Now.Ticks;

        Random.InitState(map);

        int rows = Random.Range(minRows, maxRows);

        //bool representing if we are creating a row of 11 or 10 balls
        bool newRow11 = true;

        //Initialice both lists
        List<int> row11 = new List<int>();
        for (int j = 10; j >= 0; j--)
            row11.Add(0);

        List<int> row10 = new List<int>();
        for (int j = 9; j >= 0; j--)
            row10.Add(1);

        //Initialize both the initial Graph and a list that will contain the balls of the last row(this will help to create the Graph)
        Graph initialGraph = new Graph();
        List<NormalBall> lastRowOfBalls = new List<NormalBall>();

        // 1 - 2047 -> 11 columns
        // 1 - 1023 -> 10 columns
        int bits10 = 1023, bits11 = 2047;

        int i = 0;
        while (i < rows)//Repeat every row
        {

            if (newRow11)//if we are creating a row of 11 balls
            {
                uint bitMask = (uint)Random.Range(1, bits11);//Random 11 bits

                for (int j = row11.Count-1; j >= 0; j--)//Fill the list acording to the 11 bits
                {
                    if ((bitMask >> j & 0x01) == 1)
                        row11[row11.Count - 1 - j] = 1;
                    else
                        row11[row11.Count - 1 - j] = 0;
                }
            }
            else
            {
                uint bitMask = (uint)Random.Range(1, bits10);//Random 10 bits

                for (int j = row10.Count-1; j >= 0; j--)//Fill the list acording to the 10 bits
                {
                    if ((bitMask >> j & 0x01) == 1)
                        row10[row10.Count - 1 - j] = 1;
                    else
                        row10[row10.Count - 1 - j] = 0;
                }
                   
            }

            //Try to generate the row
            if (GenerateRow(ref row11, ref row10, i, newRow11, ref lastRowOfBalls, ref initialGraph))
            {
                newRow11 = !newRow11;
                i++;
            }
        }

        initialGraph.DFSToCheckNewConnectedComponents(ref newGraphs, ref groupNeedsToFall, 1);

        return map;
    }

    /// <summary>
    /// Creates the the balls of the specified row and also builds the graph and its adyacencies
    /// </summary>
    /// <param name="row11"> The row that has 11 balls</param>
    /// <param name="row10"> The row that has 10 balls</param>
    /// <param name="rowNumber"> The number of the row that we </param>
    /// <param name="newRow11"> If we are adding a row of 11 balls</param>
    /// <param name="lastRowOfBalls"> A list with the last row of balls</param>
    /// <param name="initialGraph"> The initial graph containing all balls</param>
    /// <returns></returns>
    bool GenerateRow(ref List<int> row11, ref List<int> row10, int rowNumber, bool newRow11, ref List<NormalBall> lastRowOfBalls, ref Graph initialGraph)
    {
        //Assing references of lists to know the last row and the one to create
        List<int> lastRow = newRow11 ? row10 : row11;
        List<int> toCreate = newRow11 ? row11 : row10;

        //First check if the row can be placed
        if (!CheckIfRowCanBePlaced(ref lastRow, ref toCreate, newRow11))
            return false;

        //Max number of balls to create and init X pos
        int max = newRow11 ? row11.Count : row10.Count;
        float initPosX = newRow11 ? _initialXPos11 : _initialXPos10;

        //A List that will contain all the balls of the new row added 
        List<NormalBall> newLastRowOfBalls = new List<NormalBall>();


        //For every ball position check if we have to create it, and do it if its pertinent
        for (int i = 0; i < max; i++)
        {
            //If we have to create a ball
            if(toCreate[i] == 1)
            {
                //Instanciate a new Ball
                NormalBall tmp = Instantiate(ballPrefab, new Vector3(initPosX + (i * _xSeparation), _initialYPos - (rowNumber * _ySeparation), 0), new Quaternion(0,0,0,0), _ballContainer.gameObject.transform);

                tmp.SetCollided(true);
                tmp.SetColor((Colors)Random.Range(0, 3));
                tmp.SetBallContainer(_ballContainer);
                tmp.SetGraph(1);

                if(i == 0)//If ball is at left limit Deactivate necesary colliders
                {
                    tmp.SetEnableColliderL(false);
                    if(newRow11)
                    {
                        tmp.SetEnableColliderTL(false);
                        tmp.SetEnableColliderBL(false);
                    }
                }
                else if(i == max-1)//If ball is at right limit Deactivate necesary colliders
                {
                    tmp.SetEnableColliderR(false);
                    if (newRow11)
                    {
                        tmp.SetEnableColliderTR(false);
                        tmp.SetEnableColliderBR(false);
                    }
                }

                if (lastRowOfBalls.Count > 0)//If its not the first row
                {
                    //If adding a row of 10 balls
                    if (!newRow11 && (i + 1 < lastRow.Count && lastRow[i + 1] == 1))//Check if adyacent to topRigth ball(only in case of 10 balls)
                    {
                        tmp.AddAdyacent(lastRowOfBalls[i + 1], Dirs.TR);
                        tmp.SetEnableColliderTR(false);
                        
                        lastRowOfBalls[i + 1].AddAdyacent(tmp, Dirs.BL);
                        lastRowOfBalls[i + 1].SetEnableColliderBL(false);
                    }
                    //If adding a row of 11 balls
                    if (newRow11 && (i - 1 >= 0 && lastRow[i - 1] == 1))//Check if adyacent to topLeft ball(only in case of 11 balls)
                    {
                        tmp.AddAdyacent(lastRowOfBalls[i - 1], Dirs.TL);
                        tmp.SetEnableColliderTL(false);
                        lastRowOfBalls[i - 1].AddAdyacent(tmp, Dirs.BR);
                        lastRowOfBalls[i - 1].SetEnableColliderBR(false);
                    }

                    if (i < lastRow.Count && lastRow[i] == 1)//Check if adyacent to top ball (TopRight in case of 11 balls or TopLeft in case of 10 balls)
                    {

                        if(newRow11)
                        {
                            tmp.AddAdyacent(lastRowOfBalls[i], Dirs.TR);
                            lastRowOfBalls[i].AddAdyacent(tmp, Dirs.BL);
                            tmp.SetEnableColliderTR(false);
                            lastRowOfBalls[i].SetEnableColliderBL(false);
                        }
                        else
                        {
                            tmp.AddAdyacent(lastRowOfBalls[i], Dirs.TL);
                            lastRowOfBalls[i].AddAdyacent(tmp, Dirs.BR);
                            tmp.SetEnableColliderTL(false);
                            lastRowOfBalls[i].SetEnableColliderBR(false);
                        }
                    }
                }
                else //Else its an initial ball
                {
                    tmp.SetInitialBall(true);
                    tmp.SetEnableColliderTR(false);
                    tmp.SetEnableColliderTL(false);
                    tmp.CheckCollisionWithInitPos();
                    tmp.SetInitPosColliderEnabled(false);
                }

                if (i - 1 >= 0 && toCreate[i - 1] == 1)//Check if adyacent to left ball
                {
                    tmp.AddAdyacent(newLastRowOfBalls[newLastRowOfBalls.Count-1], Dirs.L);
                    newLastRowOfBalls[newLastRowOfBalls.Count - 1].AddAdyacent(tmp, Dirs.R);

                    tmp.SetEnableColliderL(false);
                    newLastRowOfBalls[newLastRowOfBalls.Count - 1].SetEnableColliderR(false);
                }

                //Add balls to graph and newRow
                newLastRowOfBalls.Add(tmp);
                initialGraph.AddBall(tmp);
            }
            else 
            {
                newLastRowOfBalls.Add(null);
            }
        }

        //At the end change the lastRowOfBalls
        lastRowOfBalls = newLastRowOfBalls;

        return true;
    }

    /// <summary>
    /// Checks if the row can be placed
    /// 
    /// The idea is that every connected component in a row needs to be adyacent to at least one ball in the row above
    /// So we just need a loop to check every connected component in the row
    /// </summary>
    /// <param name="lastRow"> last row that was placed</param>
    /// <param name="rowToCheck"> row that we intend to place</param>
    /// <param name="newRow11"> if we are checking a row of 11 or 10 balls</param>
    /// <returns></returns>
    bool CheckIfRowCanBePlaced(ref List<int> lastRow, ref List<int> rowToCheck, bool newRow11)
    {
        //bool to check if the connected component is adyacent to the row above
        bool adyacentTopRow = false;
        //bool to check if we are already in a connected componented
        bool inRelatedBalls = false;

        //bool that represents if the row can be placed
        bool possibleRow = true;
        //Max number of balls
        int max = newRow11 ? 11 : 10;

        for (int i = 0; i < max && possibleRow; i++)
        {
            if (rowToCheck[i] == 1)//If there is a ball in this position
            {
                //That means that we are already in a connected component or starting a new one
                inRelatedBalls = true;

                if ( i < lastRow.Count-1 && lastRow[i] == 1)//Check if adyacent to top ball (TopRight in case of 11 balls or TopLeft in case of 10 balls)
                    adyacentTopRow = true;

                if(!newRow11 && (i + 1 < lastRow.Count && lastRow[i + 1] == 1))//Check if adyacent to topRigth ball(only in case of 10 balls)
                    adyacentTopRow = true;

                if (newRow11 && (i - 1 >= 0 && lastRow[i - 1] == 1))//Check if adyacent to topLeft ball(only in case of 11 balls)
                    adyacentTopRow = true;
            }
            else if (inRelatedBalls)//If there is not a ball in this position and we were in a connected component
            {
                //That means that there is a hole in the row
                inRelatedBalls = false;
                if (!adyacentTopRow)//check if that conncected component had any adyacent to the top
                {
                    //if not, then its not posible to place the row
                    possibleRow = false;
                }
                else
                {
                    //if yes, then continue and restart the top adyacent
                    adyacentTopRow = false;
                }
            }
        }

        //Check for the case that we have a ball in the last position of the row
        if (inRelatedBalls)
        {
            inRelatedBalls = false;
            if (!adyacentTopRow)
            {
                possibleRow = false;
            }
            else
            {
                possibleRow = true;
            }
        }

        return possibleRow;
    }
        
}
