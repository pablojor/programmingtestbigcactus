﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallContainerController : MonoBehaviour
{
    [Tooltip("Speed at which the block of balls will move")]
    public float movementSpeed = 2f;

    //Target position to move
    private float _targetPosition;
    //If needs to move up
    private bool _moveUp;
    //If needs to move down
    private bool _moveDown;
    //If the game is ended
    private bool _gameEnded = false;

    //Reference to the level manager
    private LevelManager _levelManager;

    //Minimun Position were the ballContainer can be placed
    private float minPos = 10f;
    //Minimun Position were the ballContainer can be placed
    private float posToMove = 0.5f;

    //number of red balls
    private int redBalls = 0;
    private int destroyedRedBalls = 0;
    //number of blue balls
    private int blueBalls = 0;
    private int destroyedBlueBalls = 0;
    //number of green balls
    private int greenBalls = 0;
    private int destroyedGreenBalls = 0;
    //Private int 

    private void Update()
    {
        //Checks if need to be move up and moves
        if (_moveUp && transform.position.y < _targetPosition)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + (movementSpeed * Time.deltaTime), transform.position.z);
            if (transform.position.y >= _targetPosition)
                _moveUp = false;
        }
        else
            _moveUp = false;
        //Checks if need to be move down and moves
        if (_moveDown && transform.position.y > _targetPosition)
        {
            if (this.transform.position.y <= minPos || transform.position.y <= _targetPosition)
                _moveDown = false;
            else
                transform.position = new Vector3(transform.position.x, transform.position.y - (movementSpeed * Time.deltaTime), transform.position.z);
        }
        else
            _moveDown = false;
    }
    /// <summary>
    /// Calculates the position where needs to be moved
    /// </summary>
    /// <param name="Graphs"> list of Graphs</param>
    public void MoveUpStartLevel(ref List<Graph> Graphs)
    {
        _moveUp = true;
        float lowestPosition = 10000f;//Initial lowest position
        for (int i = 0; i < Graphs.Count; i++)
        {
            List<NormalBall> graphBalls = Graphs[i].GetBalls();
            for (int j = 0; j < graphBalls.Count; j++)//For every ball in every graph check the lowest ball
            {
                if (graphBalls[j].transform.position.y < lowestPosition)
                    lowestPosition = graphBalls[j].transform.position.y;

                if (graphBalls[j].GetColor() == Colors.Red)//If we find balls of some color add them
                    redBalls++;
                if (graphBalls[j].GetColor() == Colors.Blue)
                    blueBalls++;
                if (graphBalls[j].GetColor() == Colors.Green)
                    greenBalls++;

            }
        }

        if (lowestPosition < posToMove)//Calculate target position to move
            _targetPosition = transform.position.y + posToMove - lowestPosition;
        else
            _moveUp = false;
    }
    /// <summary>
    /// Calculates the position where needs to be moved
    /// </summary>
    public void MoveUp()
    {
        _moveUp = true;
        List<Graph> graphs = _levelManager.GetGraphs();
        float lowestPosition = GetLowestPosition(ref graphs);

        if (lowestPosition < posToMove)
            _targetPosition = transform.position.y + posToMove - lowestPosition;
        else
            _moveUp = false;
    }
    /// <summary>
    /// Calculates the position where needs to be moved
    /// </summary>
    public void MoveDown()
    {
        if (this.transform.position.y <= 10f)
            return;
        _moveDown = true;
        List<Graph> graphs = _levelManager.GetGraphs();

        float lowestPosition = GetLowestPosition(ref graphs);

        if (lowestPosition > -posToMove)
            _targetPosition = transform.position.y - (lowestPosition - posToMove);
        else
            _moveDown = false;
    }
    /// <summary>
    /// Calculates the lowest position of a ball
    /// </summary>
    private float GetLowestPosition(ref List<Graph> graphs)
    {
        float lowestPosition = 10000f;
        for (int i = 0; i < graphs.Count; i++)
        {
            List<NormalBall> graphBalls = graphs[i].GetBalls();
            for (int j = 0; j < graphBalls.Count; j++)
            {
                if (graphBalls[j].transform.position.y < lowestPosition)
                    lowestPosition = graphBalls[j].transform.position.y;
            }
        }

        return lowestPosition;
    }
    /// <summary>
    /// Adds a new ball to the group of graphs
    /// </summary>
    /// <param name="newBall"> the ball to be added</param>
    public void AddBallToGraph(NormalBall newBall)
    {

        List<Graph> graphs = _levelManager.GetGraphs();//Get all Graphs

        int layerMask = 1 << LayerMask.NameToLayer("StaticBall") | 1 << LayerMask.NameToLayer("InitPos"); //LayerMask for raycast(StaticBall or initial positions)

        List<Vector2> dirs = new List<Vector2>() { new Vector2(0.5f, 0.85f), new Vector2(-0.5f, 0.85f), new Vector2(1f, 0), new Vector2(-1f, 0),//Directions where you can have adyacents
                                                new Vector2(0.5f, -0.85f), new Vector2(-0.5f, -0.85f) };

        List<int> adyacentToGraphs = new List<int>();//Every Graph that the new ball is adyacent to

        for (int i = 0; i < dirs.Count; i++)//For every diretion CheckIf has an adyacent
        {
            int graph = CheckBallAdyacent(dirs[i], layerMask, newBall, i);//Get the graph of the adyacent ball

            if (graph != -1 && !CheckGraphAlreadyAdded(ref adyacentToGraphs, graph))
                adyacentToGraphs.Add(graph);
        }

        newBall.gameObject.layer = LayerMask.NameToLayer("StaticBall");//Change the layer of the object

        if (newBall.GetColor() == Colors.Red)//Add color of the new ball added
            redBalls++;
        else if (newBall.GetColor() == Colors.Blue)
            blueBalls++;
        else
            greenBalls++;

        if (adyacentToGraphs.Count == 0)//If adyacent to 0 graphs that means that is a new inital ball and a new graph must be created
        {
            Graph newGraph = new Graph();
            newGraph.SetID(graphs[graphs.Count-1].GetID() + 1);//Set id of the new graph
            newGraph.AddBall(newBall);
            newBall.SetGraph(newGraph.GetID());
            graphs.Add(newGraph);
        }
        else
        {
            if (adyacentToGraphs.Count > 1)//If adyacent to 2 or more graphs join them
                JoinGraphs(ref adyacentToGraphs, ref graphs);

            int firtGraphIndex = FindGraphIndex(adyacentToGraphs[0], ref graphs);
            graphs[firtGraphIndex].AddBall(newBall);//Add the new ball to the first graph(the other ones were merged into this one)
            newBall.SetGraph(adyacentToGraphs[0]);

            List<NormalBall> sameColorBalls = new List<NormalBall>();
            graphs[firtGraphIndex].GetBallsOfSameColor(newBall.GetColor(), newBall, ref sameColorBalls);//Get the balls of the same color adyacent with them

            if (sameColorBalls.Count > 2)//If more than 2, destroy those balls and check if any new graph was created in the separation
            {
                DestroyBallsOfSameColor(ref sameColorBalls, ref graphs, firtGraphIndex);
                CheckIfThereAreNewGraphs(ref graphs, firtGraphIndex);

                MoveDown();//Move the ball container down if necessary
            }
            else
            {
                MoveUp();//Move the ball container up if necessary
            }
        }
        if (redBalls == 0 && blueBalls == 0 && greenBalls == 0)
            EndGame(true);
    }
    /// <summary>
    /// Checks if a ball has adyacents
    /// </summary>
    /// <param name="dir"> direction to be ckecked</param>
    /// <param name="layerMask"> layer of the objects to be checked</param>
    /// <param name="newBall"> The new ball</param>
    /// <param name="dirIndex"> the index of the posible directions</param>
    /// <returns></returns>
    private int CheckBallAdyacent(Vector2 dir, int layerMask, NormalBall newBall, int dirIndex)
    {
        RaycastHit2D hit = Physics2D.Raycast(newBall.transform.position, dir.normalized, dir.magnitude, layerMask);//Raycast from the new ball to the position
        int graph = -1;
        if (hit.collider != null)
        {
            if (hit.collider.gameObject.layer == LayerMask.NameToLayer("StaticBall"))//If a ball was hit
            {
                NormalBall tmp = hit.collider.gameObject.GetComponent<NormalBall>();

                if (dirIndex == 0)//Add adyacents and disable colliders related to that direction
                {
                    newBall.AddAdyacent(tmp, Dirs.TR);
                    tmp.AddAdyacent(newBall, Dirs.BL);
                    newBall.SetEnableColliderTR(false);
                    tmp.SetEnableColliderBL(false);
                }
                else if (dirIndex == 1)
                {
                    newBall.AddAdyacent(tmp, Dirs.TL);
                    tmp.AddAdyacent(newBall, Dirs.BR);
                    newBall.SetEnableColliderTL(false);
                    tmp.SetEnableColliderBR(false);
                }
                else if (dirIndex == 2)
                {
                    newBall.AddAdyacent(tmp, Dirs.R);
                    tmp.AddAdyacent(newBall, Dirs.L);
                    newBall.SetEnableColliderR(false);
                    tmp.SetEnableColliderL(false);
                }
                else if (dirIndex == 3)
                {
                    newBall.AddAdyacent(tmp, Dirs.L);
                    tmp.AddAdyacent(newBall, Dirs.R);
                    newBall.SetEnableColliderL(false);
                    tmp.SetEnableColliderR(false);
                }
                else if (dirIndex == 4)
                {
                    newBall.AddAdyacent(tmp, Dirs.BR);
                    tmp.AddAdyacent(newBall, Dirs.TL);
                    newBall.SetEnableColliderBR(false);
                    tmp.SetEnableColliderTL(false);
                }
                else if (dirIndex == 5)
                {
                    newBall.AddAdyacent(tmp, Dirs.BL);
                    tmp.AddAdyacent(newBall, Dirs.TR);
                    newBall.SetEnableColliderBL(false);
                    tmp.SetEnableColliderTR(false);
                }

                graph = tmp.GetGraph();//Get the graph of the adyacent ball
            }
            else if (hit.collider.gameObject.layer == LayerMask.NameToLayer("InitPos"))//If its not a ball then its an initial position
            {
                //Deactivate Top colliders
                newBall.SetEnableColliderTR(false);
                newBall.SetEnableColliderTL(false);

                newBall.SetInitialBall(true);
                newBall.SetInitPosCollider(hit.collider);
                newBall.SetInitPosColliderEnabled(false);
            }
            if (newBall.transform.position.x > 4.4f)//If the ball at right position disable right colliders
            {
                newBall.SetEnableColliderR(false);
                if (newBall.transform.position.x > 4.9f)
                {
                    newBall.SetEnableColliderBR(false);
                    newBall.SetEnableColliderTR(false);
                }
            }
            else if (newBall.transform.position.x < -4.4f)//If the ball at left position disable left colliders
            {
                newBall.SetEnableColliderL(false);
                if (newBall.transform.position.x < -4.9f)
                {
                    newBall.SetEnableColliderBL(false);
                    newBall.SetEnableColliderTL(false);
                }
            }
        }
        return graph;
    }
    /// <summary>
    /// Checks if a graph was already added to a list
    /// </summary>
    /// <param name="adyacentToGraphs"> list of graphs</param>
    /// <param name="newGraph"> graph to check if was added</param>
    /// <returns></returns>
    private bool CheckGraphAlreadyAdded(ref List<int> adyacentToGraphs, int newGraph)
    {
        bool added = false;
        for (int i = 0; i < adyacentToGraphs.Count && !added; i++)
        {
            if (adyacentToGraphs[i] == newGraph)
                added = true;
        }

        return added;
    }

    /// <summary>
    /// Merges the given Graphs in to the first one
    /// </summary>
    /// <param name="adyacentToGraphs"> graphs to be merged</param>
    /// <param name="graphs"> all graphs</param>
    private void JoinGraphs(ref List<int> adyacentToGraphs, ref List<Graph> graphs)
    {
        int firtGraphIndex = FindGraphIndex(adyacentToGraphs[0], ref graphs);
        for (int i = 1; i < adyacentToGraphs.Count; i++)//For every graph to be merged
        {
            int graphIndex = FindGraphIndex(adyacentToGraphs[i], ref graphs);
            List<NormalBall> balls = graphs[graphIndex].GetBalls();//Get his balls

            for (int j = 0; j < balls.Count; j++)//Add every ball to the first graph
            {
                graphs[firtGraphIndex].AddBall(balls[j]);
                balls[j].SetGraph(adyacentToGraphs[0]);
            }
            graphs.RemoveAt(graphIndex);//Remove the merged graphs
        }
    }
    /// <summary>
    /// Finds the index of the given Graph ID
    /// </summary>
    /// <param name="graphID"> the ID of the graph</param>
    /// <param name="Graphs"> all graphs</param>
    /// <returns></returns>
    private int FindGraphIndex(int graphID, ref List<Graph> Graphs)
    {
        int index = -1;

        for (int i = 0; index == -1 && i < Graphs.Count; i++)
            if (Graphs[i].GetID() == graphID)
                index = i;
        return index;
    }

    /// <summary>
    /// Destroys the given list of Balls
    /// </summary>
    /// <param name="sameColorBalls"> list of balls to be destroyed</param>
    /// <param name="graphs"> all graphs</param>
    /// <param name="graphIndex"> index of the graph where the balls will be destroyed </param>
    private void DestroyBallsOfSameColor(ref List<NormalBall> sameColorBalls, ref List<Graph> graphs, int graphIndex)
    {
        for (int i = 0; i < sameColorBalls.Count; i++)//For every ball destroy it and update adyacents
        {

            graphs[graphIndex].GetBalls().Remove(sameColorBalls[i]);//Remove ball from the graph
            DestroyBall(sameColorBalls[i]);
        }
    }
    /// <summary>
    /// When a ball need to be destroyed
    /// </summary>
    /// <param name="ballToDestroy"> the ball to destroy</param>
    public void DestroyBall(NormalBall ballToDestroy)
    {
        if (ballToDestroy.GetInitialBall())//if it was an initial ball reactivate the initial collider
            ballToDestroy.SetInitPosColliderEnabled(true);

        List<NormalBall> adyacent = ballToDestroy.GetAdyacent();//Get adyacents of the ball destroyed

        for (int j = 0; j < adyacent.Count; j++)//For every addyacent
        {
            int index = adyacent[j].GetAdyacent().IndexOf(ballToDestroy);

            ReactivateColliders(adyacent[j], adyacent[j].GetAdyacentDirs()[index]);//Reactivete the colliders for new balls that can come
            adyacent[j].SubstractAdyacent(index);//Substract the ball from the adyacent list of the other balls
        }

        SubstractColorPoints(ballToDestroy);
        Destroy(ballToDestroy.gameObject);

    }
    /// <summary>
    /// Reactivates a collider from the given ball
    /// </summary>
    /// <param name="ball"> the ball</param>
    /// <param name="dir"> the position of the collider</param>
    private void ReactivateColliders(NormalBall ball, Dirs dir)
    {
        if (dir == Dirs.TR)
            ball.SetEnableColliderTR(true);
        else if (dir == Dirs.TL)
            ball.SetEnableColliderTL(true);
        else if (dir == Dirs.R)
            ball.SetEnableColliderR(true);
        else if (dir == Dirs.L)
            ball.SetEnableColliderL(true);
        else if (dir == Dirs.BR)
            ball.SetEnableColliderBR(true);
        else if (dir == Dirs.BL)
            ball.SetEnableColliderBL(true);
    }
    /// <summary>
    /// Substract the number of balls depending on the color of the ball
    /// </summary>
    /// <param name="ball"> the ball that will be destroyed</param>
    private void SubstractColorPoints(NormalBall ball)
    {
        if (ball.GetColor() == Colors.Red)//Substract the colors
        {
            redBalls--;
            destroyedRedBalls++;
        }
        else if (ball.GetColor() == Colors.Blue)
        {
            blueBalls--;
            destroyedBlueBalls++;
        }
        else
        {
            greenBalls--;
            destroyedGreenBalls++;
        }
    }
    /// <summary>
    /// Checks if new graphs were created after destroying balls
    /// </summary>
    /// <param name="graphs"> all graphs</param>
    /// <param name="graphIndex"> index of the graph to check </param>
    private void CheckIfThereAreNewGraphs(ref List<Graph> graphs, int graphIndex)
    {
            List<Graph> newGraphs = new List<Graph>();
            List<bool> groupNeedsToFall = new List<bool>();
            graphs[graphIndex].DFSToCheckNewConnectedComponents(ref newGraphs, ref groupNeedsToFall, graphs[graphs.Count - 1].GetID());//With a DFS check if a graph is now divided

            graphs.RemoveAt(graphIndex);//Remove the old graph
            for (int i = 0; i < newGraphs.Count; i++)//For every new graph check if needs to fall or join the other graphs
            {
                if (groupNeedsToFall[i])//If needs to fall
                {
                    List<NormalBall> graphBalls = newGraphs[i].GetBalls();
                    for (int j = 0; j < newGraphs[i].GetBalls().Count; j++)//Activate rigidbody and Destroy the balls of the graph
                    {
                        if (graphBalls[j].GetInitialBall())//If the ball was at initial pos Enable his initPos collider, so futere balls can collide
                            graphBalls[i].SetInitPosColliderEnabled(true);

                        //Deactivate all collider balls, put a little velocity and set the ball falling
                        graphBalls[j].SetAllColliders(false);
                        graphBalls[j].GetRigidbody2D().bodyType = RigidbodyType2D.Dynamic;
                        graphBalls[j].GetRigidbody2D().velocity = new Vector2(Random.RandomRange(-2, 2), Random.RandomRange(-2, 2));
                        graphBalls[j].GetRigidbody2D().gravityScale = 4;
                        graphBalls[j].SetFalling(true);

                        SubstractColorPoints(graphBalls[j]);
                        Destroy(graphBalls[j].gameObject, 0.5f);
                    }

                }
                else //If doesnt need to fall
                {
                    graphs.Add(newGraphs[i]);//Join other graphs
                }
            }
    }


    /// <summary>
    /// When a ball needs to be destroyed for an external reason (powerUps)
    /// </summary>
    /// <param name="ballToDestroy"> the ball to destroy</param>
    public void DestroyBallForPowerUp(NormalBall ballToDestroy)
    {
        List<Graph> graphs = _levelManager.GetGraphs();
        int graphIndex = FindGraphIndex(ballToDestroy.GetGraph(), ref graphs);
        graphs[graphIndex].GetBalls().Remove(ballToDestroy);//Remove ball from the graph

        DestroyBall(ballToDestroy);

        CheckIfThereAreNewGraphs(ref graphs, graphIndex);//Check if new graphs were created after destroying the ball

        if (redBalls == 0 && blueBalls == 0 && greenBalls == 0)
            EndGame(true);
    }
    /// <summary>
    /// When the game ends
    /// </summary>
    /// <param name="win"> if it was a win or not</param>
    public void EndGame(bool win)
    {
        _gameEnded = true;
        _levelManager.EndGame(GetPoints(), win);
    }

    //Setter
    public void SetLevelManager(LevelManager levelManager)
    {
        _levelManager = levelManager;
    }
    #region Getters
    public int GetRedBalls()
    {
        return redBalls;
    }
    public int GetBlueBalls()
    {
        return blueBalls;
    }
    public int GetGreenBalls()
    {
        return greenBalls;
    }
    public int GetPoints()
    {
        return (destroyedRedBalls * 5) + (destroyedBlueBalls * 7) + (destroyedGreenBalls * 10);
    }
    public bool GetEnded()
    {
        return _gameEnded;
    }
    public bool GetMoving()
    {
        return _moveDown | _moveUp;
    }
    #endregion
}
