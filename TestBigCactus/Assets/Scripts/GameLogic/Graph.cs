﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Graph
{
    private List<NormalBall> _balls;
    private int _id = -1;
    //initialize Graph
    public Graph()
    {
        _balls = new List<NormalBall>();
    }

    //Adds a ball to the graph
    public void AddBall(NormalBall ball)
    {
        _balls.Add(ball);
    }
    //Returns List of balls
    public ref List<NormalBall> GetBalls()
    {
        return ref _balls;
    }

    /// <summary>
    /// Makes a DFS and adds to the newGraphs parameter the non Connected components after doing the DFS
    /// </summary>
    /// <param name="newGraphs">List with all the new Graphs that are created after the DFS</param>
    /// <param name="groupNeedsToFall">Bool list asociated to every new Graph, that represents if there is an initial Ball in the group(first row of balls)</param>
    public void DFSToCheckNewConnectedComponents(ref List<Graph> newGraphs, ref List<bool> groupNeedsToFall, int numberOfGraphs)
    {
        //Size of the graph
        int size = _balls.Count;

        int groupOfNodes = -1;
        for (int i = 0; i < size; i++)
        {
            if(!_balls[i].GetMarkedForDFS())//If not marked there is a new group of connected componentes
            {
                groupOfNodes++;
                newGraphs.Add(new Graph());//New graph
                newGraphs[newGraphs.Count - 1].SetID(groupOfNodes + numberOfGraphs+1);
                groupNeedsToFall.Add(true);//For now, there is no initial ball

                CheckConnectedComponent(ref newGraphs, _balls[i], ref groupNeedsToFall, groupOfNodes, numberOfGraphs);
            }
        }

        //Reset all marked for DFS
        for (int i = 0; i < size; i++)
        {
            _balls[i].SetMarkedForDFS(false);
        }
    }
    /// <summary>
    /// Checks all the adyacent balls of the ball placed at index
    /// </summary>
    /// <param name="newGraph"> balls will be added to the newGraphs created</param>
    /// <param name="marked"> list of bool for marking every ball</param>
    /// <param name="groupNeedsToFall"> Will mark if a group of balls needs to fall</param>
    /// <param name="groupOfNodes"> the new graph that is being created</param>
    /// <param name="index"> index of the ball</param>
    private void CheckConnectedComponent(ref List<Graph> newGraph, NormalBall actualBall, ref List<bool> groupNeedsToFall, int groupOfNodes, int numberOfGraphs)
    {
        actualBall.SetMarkedForDFS(true);//Mark this ball
        List<NormalBall> adyacent = actualBall.GetAdyacent();//Get his adyacent

        for (int i = 0; i < adyacent.Count; i++)//For every adyacent ball if its not marked, recursive call with it
        {
            if (!adyacent[i].GetMarkedForDFS())
                CheckConnectedComponent(ref newGraph, adyacent[i], ref groupNeedsToFall, groupOfNodes, numberOfGraphs);
        }

        newGraph[groupOfNodes].AddBall(actualBall);//Add ball to his corresponding graph
        actualBall.SetGraph(groupOfNodes + numberOfGraphs+1);//Set the ID of the Graph to the ball

        if (actualBall.GetInitialBall())//If there is an initial ball in this group, it wont fall
            groupNeedsToFall[groupOfNodes] = false;
    }
    /// <summary>
    /// Makes a DFS and adds to the sameColorBalls parameter the balls of the same color that are adyacent
    /// </summary>
    /// <param name="color"> Color that the connected balls must have</param>
    /// <param name="startBall"> The ball were the DFS will start</param>
    /// <param name="sameColorBalls"> List with of the balls that are connected and are of the same color</param>
    public void GetBallsOfSameColor(Colors color, NormalBall startBall, ref List<NormalBall> sameColorBalls)
    {

        CheckAdyacentOfSameColor(color, startBall, ref sameColorBalls);//Recursive call with the first ball

        for (int i = 0; i < sameColorBalls.Count; i++)//Reset all marked balls
            sameColorBalls[i].SetMarkedForDFS(false);
    }
    private void CheckAdyacentOfSameColor(Colors color, NormalBall actualBall, ref List<NormalBall> sameColorBalls)
    {
        actualBall.SetMarkedForDFS(true);//Mark this ball
        List<NormalBall> adyacent = actualBall.GetAdyacent();//Get his adyacent

        for (int i = 0; i < adyacent.Count; i++)//For every adyacent ball if its not marked and its of the same color, recursive call with it
        {
            if (!adyacent[i].GetMarkedForDFS() && adyacent[i].GetColor() == color) 
                CheckAdyacentOfSameColor(color, adyacent[i], ref sameColorBalls);

        }
        sameColorBalls.Add(actualBall);//Add the ball to the list
    }

    //Setter
    public void SetID(int id)
    {
        _id = id;
    }
    //Getter
    public int GetID()
    {
        return _id;
    }
}
