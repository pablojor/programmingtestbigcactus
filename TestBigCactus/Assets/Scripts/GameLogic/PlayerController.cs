﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    [Tooltip("Prefab of balls to generate them")]
    public NormalBall ballPrefab;
    [Tooltip("Prefab of balls to generate them")]
    public VerticalPU verticalPU;
    [Tooltip("Prefab of balls to generate them")]
    public HorizontalPU horizontalPU;
    [Tooltip("Prefab of balls to generate them")]
    public ExplosionPU explosionPU;
    [Tooltip("Object that controls the balls")]
    public BallContainerController ballContainer;
    [Tooltip("Velocity of the ball when it is launched")]
    public float ballVelocity = 10;
    [Tooltip("SpriteRenderer of the arrow that will help aiming")]
    public SpriteRenderer arrowSprite;
    [Tooltip("SpriteRenderer of the nextColorBall")]
    public SpriteRenderer nextColorSprite;
    [Tooltip("SpriteRenderer of the nextColorBall2")]
    public SpriteRenderer nextColor2Sprite;
    [Tooltip("Text of balls left")]
    public Text txtBalls;
    [Tooltip("Text of powerUps left")]
    public Text txtPowerUps;
    [Tooltip("Text of points")]
    public Text txtPoints;

    [Tooltip("Standard ball sprite")]
    public Sprite standardSprite;
    [Tooltip("VerticalPU ball sprite")]
    public Sprite spriteVerticalPU;
    [Tooltip("HorizontalPU ball sprite")]
    public Sprite spriteHorizontalPU;
    [Tooltip("ExplosionPU ball sprite")]
    public Sprite spriteExplosionPU;

    //If the mouse Buttom is being pressed
    private bool _pressing;
    //Ball that was launched
    private NormalBall _ballInTheAir = null;
    //PowerUp that was launched
    private PowerUp _powerUpInTheAir = null;
    //Number of balls left
    private int _balls = 20;

    //Number of power ups left
    private int _powerUps = 3;

    //if verticalPU was selected
    private bool _verticalPU = false;
    //if horizontallPU was selected
    private bool _horizontallPU = false;
    //if explosionPU was selected
    private bool _explosionPU = false;

    //if a power up was launched
    private bool _checkAfterPowerUp = false;

    //Color of the nextball
    private Colors _nextColor;
    //Color of the nextball2
    private Colors _nextColor2;
    //If we need to add new balls
    private bool _needNewColors = false;

    //Minimum position where the arrow will be visible
    private float minArrowPos = -5f;

    private void Start()
    {
        //First check which colors are left
        List<Colors> colorsLeft = new List<Colors>();
        if (ballContainer.GetRedBalls() > 0)
            colorsLeft.Add(Colors.Red);
        if (ballContainer.GetBlueBalls() > 0)
            colorsLeft.Add(Colors.Blue);
        if (ballContainer.GetGreenBalls() > 0)
            colorsLeft.Add(Colors.Green);

        //Then choose a random color from the ones left
        _nextColor = colorsLeft[Random.Range(0, colorsLeft.Count)];
        _nextColor2 = colorsLeft[Random.Range(0, colorsLeft.Count)];

        ChangeSpriteColor(nextColorSprite, _nextColor);
        ChangeSpriteColor(nextColor2Sprite, _nextColor2);
    }

    void Update()
    {
        if (_ballInTheAir == null && _powerUpInTheAir == null && !ballContainer.GetMoving() && !ballContainer.GetEnded())//If there is a ball in the air you cant launch another
        {
            if (_checkAfterPowerUp)//If we launched a powerUp first check that the colors of the balls in the chamber are still in the map
            {
                txtPoints.text = "Points: " + ballContainer.GetPoints().ToString();//Change text of points
                CheckAfterPowerUp();
                _checkAfterPowerUp = false;
            }
            else if (_needNewColors)//If we need to update chamber colors because we launched a normal ball
            {
                txtBalls.text = _balls.ToString();//Change text of number of balls left
                txtPoints.text = "Points: " + ballContainer.GetPoints().ToString();//Change text of points

                if (_balls <= 0)//If no balls left tell ball container that it was defeat
                    ballContainer.EndGame(false);
                else if (_balls > 1)//Otherwise fill the chamber balls
                    AddNewColors();

                _needNewColors = false;
            }
            else if (!_pressing && Input.GetMouseButton(0))//If we press the mouseButton enable arrowSprite
            {
                _pressing = true;
                Vector3 worldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                if (worldPosition.y > minArrowPos)
                {
                    arrowSprite.gameObject.SetActive(true);
                }
            }
            else if (_pressing && Input.GetMouseButton(0))//If we are holding the mouse button move the arrow
                MoveArrow();

            else if (_pressing && !Input.GetMouseButton(0) && !_verticalPU && !_horizontallPU && !_explosionPU)//If we stop holding the mouse button launch a ball(and its not a powerUP)
                LaunchBall();

            else if (_pressing && !Input.GetMouseButton(0) && (_verticalPU || _horizontallPU || _explosionPU))//If we stop holding the mouse button and power UP is sleceted launch a powerUp
                LaunchPowerUp();
        }
        else if (_ballInTheAir != null &&_ballInTheAir.GetCollided())//If the ball in the air has collided forget it
        {
            _ballInTheAir = null;
        }
    }
    private void LaunchPowerUp()
    {
        Vector3 worldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        if (worldPosition.y > minArrowPos)
        {
            //Instanciate a prefab depending on the power UP that was selected
            if (_verticalPU)
            {
                _powerUpInTheAir = Instantiate(verticalPU, this.transform.position, this.transform.rotation, ballContainer.transform);
                _verticalPU = false;
            }
            else if (_horizontallPU)
            {
                _powerUpInTheAir = Instantiate(horizontalPU, this.transform.position, this.transform.rotation, ballContainer.transform);
                _horizontallPU = false;
            }
            else if (_explosionPU)
            {
                _powerUpInTheAir = Instantiate(explosionPU, this.transform.position, this.transform.rotation, ballContainer.transform);
                _explosionPU = false;
            }

            _powerUps--;//Substract 1 powerUp
            _checkAfterPowerUp = true;
            txtPowerUps.text = _powerUps.ToString();//Update text

            Vector2 dir = new Vector2(worldPosition.x - this.transform.position.x, worldPosition.y - this.transform.position.y);//Calculate direction

            float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            angle -= 90;
            _powerUpInTheAir.gameObject.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);//Rotate ball to mouse direction

            _powerUpInTheAir.GetRigidbody2D().constraints = RigidbodyConstraints2D.FreezeRotation;
            _powerUpInTheAir.GetRigidbody2D().bodyType = RigidbodyType2D.Dynamic;
            _powerUpInTheAir.GetRigidbody2D().velocity = dir.normalized * ballVelocity;
            _powerUpInTheAir.SetBallContainer(ballContainer);
        }

        arrowSprite.gameObject.SetActive(false);
        _pressing = false;
    }

    /// <summary>
    /// Launches a ball to a specific position
    /// </summary>
    private void LaunchBall()
    {
        Vector3 worldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        if (worldPosition.y > minArrowPos)
        {
            NormalBall tmp = Instantiate(ballPrefab, this.transform.position, this.transform.rotation, ballContainer.transform);//Instanciate ball
            _balls--;
            _needNewColors = true;

            Vector2 dir = new Vector2(worldPosition.x - this.transform.position.x, worldPosition.y - this.transform.position.y);//Calculate direction

            _ballInTheAir = tmp;

            float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            angle -= 90;
            tmp.gameObject.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);//Rotate to mouse direction

            _ballInTheAir.SetColor(_nextColor);

            tmp.GetRigidbody2D().constraints = RigidbodyConstraints2D.FreezeRotation;
            tmp.GetRigidbody2D().bodyType = RigidbodyType2D.Dynamic;
            tmp.GetRigidbody2D().velocity = dir.normalized * ballVelocity;

            _ballInTheAir.SetBallContainer(ballContainer);
            _ballInTheAir.SetCollided(false);
            _ballInTheAir.gameObject.layer = LayerMask.NameToLayer("FlyingBall");

            //Disable colliders
            _ballInTheAir.SetEnableColliderTR(false);
            _ballInTheAir.SetEnableColliderTL(false);
            _ballInTheAir.SetEnableColliderR(false);
            _ballInTheAir.SetEnableColliderL(false);
            _ballInTheAir.SetEnableColliderBR(false);
            _ballInTheAir.SetEnableColliderBL(false);

            //Enable head collider
            _ballInTheAir.SetHeadCollider(true);

            //Second chamber is black until the ball has collided
            _nextColor = _nextColor2;
            nextColor2Sprite.color = Color.black;
            ChangeSpriteColor(nextColorSprite, _nextColor);
        }
        _pressing = false;

        arrowSprite.gameObject.SetActive(false);
    }
    /// <summary>
    /// Moves the arrow to help aiming
    /// </summary>
    private void MoveArrow()
    {
        Vector3 worldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        if (worldPosition.y < minArrowPos) //If under a certain point deactivate arrow
            arrowSprite.gameObject.SetActive(false);
        else//Otherwise rotate
        {
            arrowSprite.gameObject.SetActive(true);
            Vector2 dir = new Vector2(worldPosition.x - this.transform.position.x, worldPosition.y - this.transform.position.y);
            float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            angle -= 90;
            arrowSprite.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
    }
    /// <summary>
    /// Changes the colors in the chamber if needed after launching a powerUP
    /// </summary>
    private void CheckAfterPowerUp()
    {
        bool change1 = false, change2 = false;

        List<Colors> colorsLeft = new List<Colors>();
        if (ballContainer.GetRedBalls() <= 0)//If no red left and we had a red ball in the chamber
        {
            if (_nextColor == Colors.Red)
                change1 = true;
            if (_nextColor2 == Colors.Red)
                change2 = true;
        }
        else
            colorsLeft.Add(Colors.Red);
        if (ballContainer.GetBlueBalls() <= 0)//If no blue left and we had a red ball in the chamber
        {
            if (_nextColor == Colors.Blue)
                change1 = true;
            if (_nextColor2 == Colors.Blue)
                change2 = true;
        }
        else
            colorsLeft.Add(Colors.Blue);
        if (ballContainer.GetGreenBalls() <= 0)//If no green left and we had a red ball in the chamber
        {
            if (_nextColor == Colors.Green)
                change1 = true;
            if (_nextColor2 == Colors.Green)
                change2 = true;
        }
        else
            colorsLeft.Add(Colors.Green);

        if (change1)//If chamber ball color is no longer in the map choose a new one
            _nextColor = colorsLeft[Random.Range(0, colorsLeft.Count)];

        if (change2)//If chamber ball2 color is no longer in the map choose a new one
        {
            _nextColor2 = colorsLeft[Random.Range(0, colorsLeft.Count)];
            ChangeSpriteColor(nextColor2Sprite, _nextColor2);
        }
        //Always put back the normal sprite in the firs ball in the chamber
        nextColorSprite.sprite = standardSprite;
        ChangeSpriteColor(nextColorSprite, _nextColor);
    }
    /// <summary>
    /// Changes the colors of the balls in the chamber
    /// </summary>
    private void AddNewColors()
    {
        bool changeBoth = false;
        List<Colors> colorsLeft = new List<Colors>();

        if (ballContainer.GetRedBalls() > 0)
            colorsLeft.Add(Colors.Red);
        else if (_nextColor == Colors.Red)
            changeBoth = true;

        if (ballContainer.GetBlueBalls() > 0)
            colorsLeft.Add(Colors.Blue);
        else if (_nextColor == Colors.Blue)
            changeBoth = true;

        if (ballContainer.GetGreenBalls() > 0)
            colorsLeft.Add(Colors.Green);
        else if (_nextColor == Colors.Green)
            changeBoth = true;

        if (changeBoth)//If there are no balls left of a specific type change both chamber balls
        {
            _nextColor = colorsLeft[Random.Range(0, colorsLeft.Count)];
            ChangeSpriteColor(nextColorSprite, _nextColor);
        }

        _nextColor2 = colorsLeft[Random.Range(0, colorsLeft.Count)];

        ChangeSpriteColor(nextColor2Sprite, _nextColor2);
        _needNewColors = false;
    }

    /// <summary>
    /// Changes the color of the sprite (of the chamber balls)
    /// </summary>
    /// <param name="ball"> sprite to change</param>
    /// <param name="color"> color to change</param>
    private void ChangeSpriteColor(SpriteRenderer ball, Colors color)
    {
        if (color == Colors.Red)
            ball.color = Color.red;
        else if (color == Colors.Blue)
            ball.color = Color.blue;
        else if (color == Colors.Green)
            ball.color = Color.green;
    }

    /// <summary>
    /// Selects Vertical power up if there are power ups left
    /// </summary>
    public void SetVertical()
    {
        if (_powerUps <= 0)
            return;
        nextColorSprite.sprite = spriteVerticalPU;//Change sprite of the first chamber ball
        nextColorSprite.color = Color.white;

        _verticalPU = true;
        _horizontallPU = false;
        _explosionPU = false;
    }
    /// <summary>
    /// Selects Horizontal power up if there are power ups left
    /// </summary>
    public void SetHorizontal()
    {
        if (_powerUps <= 0)
            return;
        nextColorSprite.sprite = spriteHorizontalPU;//Change sprite of the first chamber ball
        nextColorSprite.color = Color.white;

        _verticalPU = false;
        _horizontallPU = true;
        _explosionPU = false;
    }
    /// <summary>
    /// Selects Explosion power up if there are power ups left
    /// </summary>
    public void SetExplosion()
    {
        if (_powerUps <= 0)
            return;
        nextColorSprite.sprite = spriteExplosionPU;//Change sprite of the first chamber ball
        nextColorSprite.color = Color.white;

        _verticalPU = false;
        _horizontallPU = false;
        _explosionPU = true;
    }
}
