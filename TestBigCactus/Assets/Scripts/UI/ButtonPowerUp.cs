﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonPowerUp : MonoBehaviour
{
    public PlayerController player;

    public void OnClickV()
    {
        player.SetVertical();
    }
    public void OnClickH()
    {
        player.SetHorizontal();
    }
    public void OnClickE()
    {
        player.SetExplosion();
    }
}
