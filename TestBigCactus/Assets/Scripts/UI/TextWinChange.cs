﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextWinChange : MonoBehaviour
{
    public Text winText;
    void Awake()
    {
        if (GameManager.GetInstance().GetWinLastGame())
            winText.text = "Victory";
        else
            winText.text = "Defeat";
    }
}
