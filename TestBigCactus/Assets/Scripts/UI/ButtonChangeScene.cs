﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonChangeScene : MonoBehaviour
{
    [Tooltip("Name of the scene to change")]
    public string Scene;
    public void OnClick()
    {
        SceneManager.LoadScene(Scene);
    }
}
