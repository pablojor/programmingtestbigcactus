﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonPlayAgain : MonoBehaviour
{
    [Tooltip("Name of the scene to change")]
    public string Scene;
    public void OnClick()
    {
        GameManager.GetInstance().PlayLastGame();
        SceneManager.LoadScene(Scene);
    }
}
