﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    [Tooltip("Script that generates a new Map")]
    public MapGenerator mapGenerator;
    [Tooltip("GameObject where the balls created will be placed")]
    public BallContainerController ballContainer;

    private List<Graph> Graphs;

    private int _actualMap;

    /// <summary>
    /// initialize the list of Graphs
    /// </summary>
    public void Init()
    {
        Graphs = new List<Graph>();
        ballContainer.SetLevelManager(this);
    }

    /// <summary>
    /// loads a new level
    /// </summary>
    public void LoadMap(int map = 0)
    {
        List<bool> groupNeedsToFall = new List<bool>();
        _actualMap = mapGenerator.ProceduralGeneratedMap(ref Graphs, ref groupNeedsToFall, ballContainer, map);

        //Check if the container need to move
        ballContainer.MoveUpStartLevel(ref Graphs);
    }
    /// <summary>
    /// Saves the score in to PlayerPrefs and tells the game Manager that the level ended
    /// </summary>
    /// <param name="points"> points you achieved</param>
    public void EndGame(int points, bool win)
    {
        PlayerPrefs.SetInt(_actualMap.ToString() + "actual", points);
        int max = PlayerPrefs.GetInt(_actualMap.ToString() + "max", -1);

        if(max < points)
            PlayerPrefs.SetInt(_actualMap.ToString() + "max", points);

        GameManager.GetInstance().EndGame(_actualMap,win);
    }
    //Getter
    public ref List<Graph> GetGraphs()
    {
        return ref Graphs;
    }
}
