﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraController : MonoBehaviour
{
    [Tooltip("Camera in Game Scene")]
    public Camera MainCamera;
    [Tooltip("Canvas to rescale")]
    public CanvasScaler Canvas;

    /// <summary>
    /// Changes the size of the camera if needed
    /// </summary>
    void Start()
    {
        int targetWidthSize = 12;
        float targetWindowRatio = 9f / 16f;

        float windowaspect = (float)Screen.width / (float)Screen.height;

        windowaspect = Mathf.Round(windowaspect * 100f) / 100f;
        if (windowaspect <= targetWindowRatio)
        {
            MainCamera.orthographicSize = (targetWidthSize * ((float)Screen.height / (float)Screen.width)) / 2;
            Canvas.matchWidthOrHeight = 0;
        }
        else
        {
            Canvas.matchWidthOrHeight = 1;
        }
    }

}
