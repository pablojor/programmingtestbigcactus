# ProgrammingTestBigCactus

Firstly, every content was created by me. Also i didn't see at the start the unity version.
So the first commits are in 2020.1.4f1, anyways the lastest commit is in 2019.4.2f1 version.

I spent 17~18 hours to complete the test.

I followed the idea in your Los Pitufos game. There are 11 initial position at the top where balls can be placed.
So if a group of connected balls has at least 1 ball that is in an initial position that group won't fall
otherwise that group will fall and the balls will be destroyed.

To acomplish this behaviour I used graphs and DFS. When balls are destroyed because 3 balls of the same color are
together I check if new graphs(group of balls) are created and if there is not an initial ball with them they fall.

Also the block of balls moves up like in Los Pitufos games if balls create new rows, and move down so there are always
balls in the screen.

The maps are proceduraly genereted and you can set the number of rows in the levelManager public parameters(GameScene).
When a level ends your score in that game is saved in PlayerPrefs and your maxScore in that level also updates.
To generate a level you only need a seed for the random numbers so levels can be replayed and the only memory needed
is an int to store them.

There are 3 buttons in the botton Left of the screen, representing each power UP, when you select one of them the next
ball will be a power UP.

At the botton right is the ball chamber and how many balls are left. At the botton left the current points will be 
displayed.

When a game ends you go to other screen where you can choose if you want to replay the map or play other one.

In the future a level selector should be added.
In terms of the gameplay a line like the one at Los Pitufos to show the trayectory of the ball would be perfect.

To add more powerUps it would be easy as there is a fatherClass(PowerUp) that already has most of the funcionality
because PowerUp inherits from base class Ball. So only the new behaviour of the power Up would need to be added.
